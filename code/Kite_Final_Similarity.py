import networkx as nx 
from lxml import etree as ET
import math 
import sys
from os import walk
from os.path import isfile, join
import array 
import numpy as np
from scipy.optimize import linear_sum_assignment
from os import listdir






class   Geometric_point: 
    def __init__(self, x, y): 
        self.x = x 
        self.y = y 


def read_graph_from_xml(file_path):
    G = nx.Graph()
    nodes={}
    tree = ET.parse(file_path)
    root = tree.getroot()
    id = 0
    for graph in root:
        for element in graph:
            if element.tag == "node":
                nodes[str(element.attrib['id'])]=id
                i = 0
                x_v=y_v=0
                for attrib in element:
                    for value in attrib:
                        if i==0:
                            x_v=float(value.text)
                        else:
                            y_v=float(value.text)
                        i+=1
                G.add_node(id,x=x_v,y=y_v)
                id+=1
            elif element.tag=="edge" :# it's an edge    
                source = nodes[str(element.attrib["source"])]
                target = nodes[str(element.attrib["to"])]
                distance = math.sqrt(math.pow(G.nodes[source]['x']-G.nodes[target]['x'],2) + math.pow(G.nodes[source]['y']-G.nodes[target]['y'],2))
                G.add_edge(source,target,weight=distance)
    return G


#                                        Geometric functions     
#    

def orientation(p, q, r): 
    # to find the orientation of an ordered triplet (p,q,r) 
    # function returns the following values: 
    # 0 : Colinear points 
    # 1 : Clockwise points 
    # 2 : Counterclockwise 
      
    # Source :   https://www.geeksforgeeks.org/orientation-3-ordered-points/amp/  
      
    val = (float(q.y - p.y) * (r.x - q.x)) - (float(q.x - p.x) * (r.y - q.y)) 
    if (val > 0): 
        # Clockwise orientation 
        return 1
    elif (val < 0): 
          
        # Counterclockwise orientation 
        return 2
    else:   
        # Colinear orientation 
        return 0

def onSegment(p, q, r): 
    if ( (q.x <= max(p.x, r.x)) and (q.x >= min(p.x, r.x)) and 
           (q.y <= max(p.y, r.y)) and (q.y >= min(p.y, r.y))): 
        return True
    return False


def angle_between(x1,y1,x2,y2):
    angle = math.atan2(x1*y2-y1*x2,x1*x2+y1*y2)
    if (angle < 0 ):
        angle += 2*math.pi
    return angle

def angle_between_3_points(G,p1,p2,p3):
    a = math.sqrt(math.pow(G.nodes[p1]['x']-G.nodes[p3]['x'],2) + math.pow(G.nodes[p1]['y']-G.nodes[p3]['y'],2)) 
    b = math.sqrt(math.pow(G.nodes[p2]['x']-G.nodes[p3]['x'],2) + math.pow(G.nodes[p2]['y']-G.nodes[p3]['y'],2)) 
    c = math.sqrt(math.pow(G.nodes[p1]['x']-G.nodes[p2]['x'],2) + math.pow(G.nodes[p1]['y']-G.nodes[p2]['y'],2))
    d = round((b*b+c*c-a*a)/(2*b*c),4)
    ang_rd = math.acos(d)
    return ang_rd

def doIntersect(p1,q1,p2,q2): 
    # Find the 4 orientations required for  
    # the general and special cases 
    o1 = orientation(p1, q1, p2) 
    o2 = orientation(p1, q1, q2) 
    o3 = orientation(p2, q2, p1) 
    o4 = orientation(p2, q2, q1) 
  
    # General case 
    if ((o1 != o2) and (o3 != o4)): 
        return True
  
    # Special Cases 
  
    # p1 , q1 and p2 are colinear and p2 lies on segment p1q1 
    if ((o1 == 0) and onSegment(p1, p2, q1)): 
        return True
    # p1 , q1 and q2 are colinear and q2 lies on segment p1q1 
    if ((o2 == 0) and onSegment(p1, q2, q1)): 
        return True
    # p2 , q2 and p1 are colinear and p1 lies on segment p2q2 
    if ((o3 == 0) and onSegment(p2, p1, q2)): 
        return True
    # p2 , q2 and q1 are colinear and q1 lies on segment p2q2 
    if ((o4 == 0) and onSegment(p2, q1, q2)): 
        return True
    # If none of the cases 
    return False
def euclidean_distance(P,Q):
    return math.sqrt(math.pow(P[0]-Q[0],2)+math.pow(P[1]-Q[1],2))



def enclosure_embedding(cord_list):
    # we use this notation 
    # s : same direction 
    # o : change in the direction of one coordinates 
    # t : change in the direction of two coordinates
    orientation_embd = []
    list_direction = []
    for i in range(len(cord_list)-1):
        x_direction =  '='
        y_direction =  '='
        if ( cord_list[i+1][0]> cord_list[i][0] ) : 
            x_direction = '+'
        elif ( cord_list[i+1][0]< cord_list[i][0]  ):
            x_direction = '-'
        if ( cord_list[i+1][1]> cord_list[i][1] ) : 
            y_direction = '+'
        elif ( cord_list[i+1][1]< cord_list[i][1]  ):
            y_direction = '-'
        list_direction.append((x_direction,y_direction))
    for i in range(len(list_direction)-1):
        x1,y1 = list_direction[i]
        x2,y2 = list_direction[i+1]
        c = ' '
        if (x1==x2) and (y1==y2) : 
            c = 's'
        elif (x1!=x2) and (y1!=y2):
            c = 't'
        else :
            c = 'o'
        orientation_embd.append(c)
    return orientation_embd 

def cost_subtitution(a,b):
    if a == b : 
        return 0 
    else:
        if (a=='o'or b=='s') or ( b=='o' and a=='s') or (a=='o'or b=='t') or (a=='t'or b=='o') :
            return 0.5
        else : 
            return 1

def string_edit_distance(S1,S2):
    """ Compute the minimum edit distance
    between two strings using the
    Wagner-Fischer algorithm (Dynamic programming)"""

    # Create (m+1)x(n+1) matrix
    cost_matrix = [ [ 0 for j in range(0, len(S2) +1)] 
              for i in range(0, len(S1) +1) 
            ]
    # Initialisation
    for i in range(0, len(S1) +1):
        if i == 0:
            cost_matrix[i][0] = 0
        else:
            cost_matrix[i][0] = 1 + cost_matrix[i-1][0]

    # Initialisation
    for j in range(0, len(S2) +1):
        if j==0:
            cost_matrix[0][j] = 0
        else:
            cost_matrix[0][j] = 1 + cost_matrix[0][j-1]
    for i in range(1, len(S1) +1):
        for j in range(1, len(S2) +1):
            S1Index = i - 1
            S2Index = j - 1
            costs= [ cost_matrix[i][j-1] + 1,
                     cost_matrix[i-1][j] +  1,
                     cost_matrix[i-1][j-1] + cost_subtitution(S1[S1Index],S2[S2Index])
                   ]
            costs.sort()
            cost_matrix[i][j] = costs[0]
    return cost_matrix[len(S1)][len(S2)]


def enclosure_radius(enc_poly):
    radius  = 0
    for p in enc_poly:
        for q in enc_poly:
            d = euclidean_distance(p,q)
            if d > radius :
                radius = d
    return radius


#                               Kite functions                          #

def Kite_properties(kite_graph) :
    edges  = list(kite_graph.edges())
    enclosure_polygon = []
    ratio_antenna_enclosure  = 0
    # computing enclosure shape embeding 
    cercles = []
    for i in range(len(edges)-2) : 
        p1  = Geometric_point( kite_graph.nodes[edges[i][0]]['x'],  kite_graph.nodes[edges[i][0]]['y'])
        q1  =  Geometric_point( kite_graph.nodes[edges[i][1]]['x'] ,  kite_graph.nodes[edges[i][1]]['y'])
        for  j  in range(i+2,len(edges)) :
            p2  = Geometric_point ( kite_graph.nodes[edges[j][0]]['x'] ,  kite_graph.nodes[edges[j][0]]['y'])
            q2  = Geometric_point ( kite_graph.nodes[edges[j][1]]['x'] , kite_graph.nodes[edges[j][1]]['y'])
            if  doIntersect(p1,q1,p2,q2):
                cercles.append([edges[i][0],edges[j][1]])
    enclosure_start_node = cercles[0][0]
    enclosure_end_node =   cercles[len(cercles)-1][1]
    for  i  in range(len(cercles)-1):
        enclosure_polygon.append([kite_graph.nodes[cercles[i][1]]['x'],  kite_graph.nodes[cercles[i][1]]['y']])
        mid_point = int( (cercles[i][1]+cercles[i+1][0])/2)
        enclosure_polygon.append( [kite_graph.nodes[mid_point]['x'],  kite_graph.nodes[mid_point]['y']])
    enclosure_polygon.append([ kite_graph.nodes[enclosure_end_node]['x'],  kite_graph.nodes[enclosure_end_node]['y']])
    enclosure_vec = enclosure_embedding(enclosure_polygon)    
    #computing twist  angles 
    twist_angle_1 = 180
    twist_angle_2 = 180
    for i in range(1,enclosure_start_node-2):
        #if twist_angle_1 >   math.degrees (angle_between_3_points(kite_graph,i,i+1,i+2)) :
        twist_angle_1 += math.degrees (angle_between_3_points(kite_graph,i,i+1,i+2))
    twist_angle_1 = twist_angle_1 % 180
    end_point   = len(list(kite_graph.nodes()))
    for i in range(enclosure_end_node,end_point-2):
        #if twist_angle_2 >   math.degrees (angle_between_3_points(kite_graph,i,i+1,i+2)) :
        twist_angle_2 += math.degrees (angle_between_3_points(kite_graph,i,i+1,i+2))
    twist_angle_2 = twist_angle_2 % 180



    # computing  enclosure radius / antena length ratio 
    antenna_lenght = nx.shortest_path_length(kite_graph,source=0,target=enclosure_start_node,weight="weight")
    antenna_lenght += nx.shortest_path_length(kite_graph,source=enclosure_end_node,target=end_point-1,weight="weight")
    
    enclosure_length = nx.shortest_path_length(kite_graph,source=enclosure_start_node,target=enclosure_end_node,weight="weight")
    ratio_antenna_enclosure =  enclosure_length / antenna_lenght
    
    return enclosure_vec, twist_angle_1,twist_angle_2,ratio_antenna_enclosure





def polygon_similarity(pol1,pol2):
    dist = string_edit_distance(pol1,pol2)
    return (max(len(pol1),len(pol2)) -dist) / max(len(pol1),len(pol2))

def kite_similarity(Gk1,Gk2,w1,w2,w3):

    e_p1,t11,t12,r_a_e1 =  Kite_properties(Gk1)
    e_p2,t21,t22,r_a_e2 =  Kite_properties(Gk2)

    sim_polyg = polygon_similarity(e_p1,e_p2)
    tw_s1 = 1 - ( abs(t11-t21)/max(t11,t21))/2 - ( abs(t12-t22)/max(t12,t22))/2
    tw_s2 = 1 - ( abs(t11-t22)/max(t11,t22))/2 - ( abs(t12-t21)/max(t12,t21))/2
    twist_similarity = max(tw_s1,tw_s2)
    enclosure_similarity = 1 - abs(r_a_e1-r_a_e2)/max(r_a_e1,r_a_e2)
    #print(sim_polyg,twist_similarity,enclosure_similarity)
    return w1*sim_polyg+w2*twist_similarity+w3*enclosure_similarity



if __name__ == "__main__":
    graphs_dir = sys.argv[1]  # graphs directory
    graph_ref  = sys.argv[2]  # protoype graph
    results_file_path = sys.argv[3]  # output file
    
    
 
    weights = [(0.6,0.2,0.2)]  # best combinaison of parameter for protoype 1    

    #weights = [(0.8,0.1,0.1)]    # best combinaison of parameter for protoype 2 
    
    files = [f for f in listdir(graphs_dir) if isfile(join(graphs_dir, f))]
    
    ref_graph = read_graph_from_xml(graph_ref)

    #results = open(join(out_dir,"ref4_results_FINAL.csv"),"w")
    results_file = open(results_file_path,"w")
    for w in weights :
        w1,w2,w3 = w
        print(w1,w2,w3)
        dict_distances = {}
        for f in files: 
            G =  read_graph_from_xml(join(graphs_dir, f))
            dist = kite_similarity(G,ref_graph,w1,w2,w3)
            dict_distances[f] =  dist
            print(f+" "+str(dist))
            results_file.write(str(f)+","+str(dist)+"\n")
