from xml.dom import minidom
import networkx as nx
from lxml import etree as ET
import sys
from os import walk
from itertools import count

def graph_to_xml(G,file_path):
    gxl = ET.Element("gxl")
    graph_xml = ET.SubElement(gxl,"graph")
    for n in G.nodes():
        node_xml = ET.SubElement(graph_xml,"node", id =str(n))
        attrib =   ET.SubElement(node_xml,"attr", name =str("x"))
        x_cord =   ET.SubElement(attrib,"float").text = str(G.nodes[n]['x'])
        attrib =   ET.SubElement(node_xml,"attr", name =str("y"))
        y_cord =   ET.SubElement(attrib,"float").text = str(G.nodes[n]['y'])
    i = 0
    for e in G.edges():
        edge_xml = ET.SubElement(graph_xml,"edge", id =str(i),source=str(e[0]),to = str(e[1]))#,weight = str(G[e[0]][e[1]]["weight"]))
        i+=1
    tree = ET.ElementTree(gxl)
    tree.write(file_path,pretty_print=False)







if __name__ == "__main__":


    original_images_path = sys.argv[1]  # original images directory
    output_graph_path = sys.argv[2]  # output graphs directory
    for (dirpath, dirname, filenames) in walk(original_images_path):
        for filename in filenames:
            print(filename)
            svg_file = original_graph_path+"\\"+filename
            doc = minidom.parse(svg_file)  # parseString also exists
            path_strings = [path.getAttribute('d') for path
                        in doc.getElementsByTagName('path')]
            doc.unlink()
            string = str(path_strings).strip('[')
            string = string.strip(']')
            string = string.split('M')
            string = str(string[1]).split('C')
            string  = str(string[1]).split('            ')
            node_id = 0
            G = nx.Graph()
            for s in string: 
                s.strip("'")
                nodes = s.split(' ')
                source = nodes[2].split(',')
                dest = nodes[3].split(',')
                if ( node_id ==0):
                    G.add_node(node_id,x= float(source[0]),y= float(source[1]))
                    node_id+=1
                    G.add_node(node_id,x= float(dest[0]),y= float(dest[1]))
                    node_id+=1
                    G.add_edge(node_id-2,node_id-1)
                else :
                    G.add_node(node_id,x= float(dest[0]),y= float(dest[1]))
                    G.add_edge(node_id-1,node_id)
                    node_id+=1
            graph_to_xml(G,output_graph_path+"\\"+filename.split(".")[0]+".gxl")
